const modalTriggers = document.querySelectorAll('.modal-trigger')
const bodyOverlay = document.querySelector('.body-overlay')
const modalCloseTriggers = document.querySelectorAll('.modal__close')
const modalContinueTrigger = document.querySelector('.modal__continue')

modalTriggers.forEach(trigger => {
    trigger.addEventListener('click', () => {
        //const { modalTrigger } = trigger.dataset // Object deconstruction and dataset not available in older browsers

        // Get trigger
        const modalTrigger = trigger.getAttribute('data-modal-trigger');

        // Get correct modal
        const modal = document.querySelector(`[data-modal="${modalTrigger}"]`)

        // Show modal and overlay
        modal.classList.add('is--visible')
        bodyOverlay.classList.add('is--black')

        // Close modal from triggers (close)
        modalCloseTriggers.forEach(trigger => {
          trigger.addEventListener('click', () => {
              modal.classList.remove('is--visible')
              bodyOverlay.classList.remove('is--black')
          })
        })

        // Close modal from trigger (continue)
        modalContinueTrigger.addEventListener('click', () => {
            modal.classList.remove('is--visible')
            bodyOverlay.classList.remove('is--black')
        })

        // Close modal from trigger (overlay)
        bodyOverlay.addEventListener('click', () => {
            modal.classList.remove('is--visible')
            bodyOverlay.classList.remove('is--black')
        })
    })
})