'use strict';

var modalTriggers = document.querySelectorAll('.modal-trigger');
var bodyOverlay = document.querySelector('.body-overlay');
var modalCloseTriggers = document.querySelectorAll('.modal__close');
var modalContinueTrigger = document.querySelector('.modal__continue');

modalTriggers.forEach(function (trigger) {
    trigger.addEventListener('click', function () {
        //const { modalTrigger } = trigger.dataset // Object deconstruction and dataset not available in older browsers

        // Get trigger
        var modalTrigger = trigger.getAttribute('data-modal-trigger');

        // Get correct modal
        var modal = document.querySelector('[data-modal="' + modalTrigger + '"]');

        // Show modal and overlay
        modal.classList.add('is--visible');
        bodyOverlay.classList.add('is--black');

        // Close modal from triggers (close)
        modalCloseTriggers.forEach(function (trigger) {
            trigger.addEventListener('click', function () {
                modal.classList.remove('is--visible');
                bodyOverlay.classList.remove('is--black');
            });
        });

        // Close modal from trigger (continue)
        modalContinueTrigger.addEventListener('click', function () {
            modal.classList.remove('is--visible');
            bodyOverlay.classList.remove('is--black');
        });

        // Close modal from trigger (overlay)
        bodyOverlay.addEventListener('click', function () {
            modal.classList.remove('is--visible');
            bodyOverlay.classList.remove('is--black');
        });
    });
});